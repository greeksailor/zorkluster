# Zorkluster - Horizontally Scalable Zork
2019-06-29

Scale to millions of users playing zork online!*

This repo provides the sources to build the **zorkluster** docker image and also the kubernetes deployment files to provide
a massively scalable web interface to play zork (the famous interactive fiction game from [Infocom](https://en.wikipedia.org/wiki/Infocom)).

*Note: While this repo is intended to be functional, it's not an entirely serious project.

## what you get
The zork web interface looks like this (after pointing your browser at http://localhost)

```
West of House                               Score: 0        Moves: 0

ZORK I: The Great Underground Empire
Copyright (c) 1981, 1982, 1983 Infocom, Inc. All rights reserved.
ZORK is a registered trademark of Infocom, Inc.
Revision 88 / Serial number 840726

West of House
You are standing in an open field west of a white house, with a boarded
front door.
There is a small mailbox here.

[your command here]
```

## docker image
The contents of this repo can be used to build a docker image (see [Dockerfile]) and there is an image (built from this repo)
on https://hub.docker.com.

## kubernetes deploy
The 2 kubernetes files below are used to start the cluster. The number of replicas of the server can be set in `zorkluster.yaml`
and the webapp hostname (default localhost) can be set in `ingress.yaml`.

* `zorkluster.yaml`	(starts the web app)
* `ingress.yaml` (provides the network endpoint to the web app)

## running locally with microk8s
I have not actually tested this in a _real_ cluster... I don't have _cluster money_.
I've only tested it locally using Canonical's `microk8s` on `arch linux (manjaro)`.
Here is how to replicate that (assuming you have installed [mikrok8s](https://microk8s.io/) - I used _snap_ to do that).

```
$ cd kubernetes
$ make microk8s-zork-up
... sudo will ask for passwd
... starts microk8s and then zorkluster
... time passes ...

$ microk8s.status
$ microk8s.status --wait-ready # blocks until ready

$ kubectl get pods
... will eventually show READY as 1/1 when everything is ready
```

Point browser to http://localhost
Play zork...

Shutting everything down again

```
$ make microk8s-zork-down
... shuts everything down in reverse order to above
```

See inside the [kubernetes/makefile] for the actual commands used.

If you're seeing this on docker hub, the source code repo is hosted at https://bitbucket.org/greeksailor/zorkluster/.

## tested on

The system has been tested and runs on the following platforms (please let me know if it works on your platform).

Browsers

  * Firefox Quantum (v67 Manjaro/Arch)
  * dillo (v3.0.5 Manjaro/Arch)
  * lynx (2.8.9rel.1 Manjaro/Arch)
  * lynx (?? Ubuntu)

Servers

  * Manjaro/ArchLinux
```
  LSB Version:  n/a
  Distributor ID: ManjaroLinux
  Description:  Manjaro Linux
  Release:  18.0.4
  Codename: Illyria
```
  * Ubuntu
```
  No LSB modules are available.
  Distributor ID: Ubuntu
  Description:    Ubuntu 18.04.2 LTS
  Release:        18.04
  Codename:       bionic
```

## credits

With thanks to all these:

* docker [https://docs.docker.com]
* kubernetes [https://kubernetes.io]
* microk8s [https://microk8s.io]
* python [https://www.python.org]
* Frotz Z-Machine (Stefan Jokisch, ported by Galen Hazelwood and David Griffith) [https://davidgriffith.gitlab.io/frotz]
* Infocom [https://en.wikipedia.org/wiki/Infocom]
